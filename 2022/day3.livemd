<!-- livebook:{"persist_outputs":true} -->

# Advent Of Code 2022 - jour 3

```elixir
Mix.install([
  {:req, "~> 0.3.2"}
])
```

<!-- livebook:{"output":true} -->

```
:ok
```

## Téléchargement de l'input

On essaie de télécharger le input personalisé, sinon on 'default' sur celui d'exemple de l'énoncé.

Pour charger l'input de votre session, vous devez aller chercher avec les 'developer tools' de votre navigateur le cookie de session 'session' en étant loggué dans https://adventofcode.com puis l'ajouter dans les secrets de ce livebook, sous 'AOC_SESSION' (voir https://news.livebook.dev/whats-new-in-livebook-0.7-2wOPsY)

```elixir
day_sample_input = """
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
"""

input =
  try do
    aoc_session = System.fetch_env!("LB_AOC_SESSION")
    url = "https://adventofcode.com/2022/day/3/input"
    Req.get!(url, headers: [cookie: "session=#{aoc_session}"]).body
  rescue
    _e -> day_sample_input
  end
```

<!-- livebook:{"output":true} -->

```
"vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw\n"
```

## Résolution de la partie 1

Chaque ligne est un sac à dos, qui contient deux compartiment de meme taille :

```elixir
step1 =
  input
  |> String.split("\n", trim: true)
  |> Enum.map(&(&1 |> String.split_at(String.length(&1) |> div(2))))
  |> Enum.map(&Tuple.to_list/1)

  # Pour trouver ce qui est commun au deux, il suffit d'utiliser 
  # l'opérateur -- de liste : 
  # list1 = [1,2,3]
  # list2 = [4,5,2]
  # list1 -- (list1 -- list2)
  # => [2]
  # Ce qu'on fera après avoir transformer en char list
  |> Enum.map(fn sac -> Enum.map(sac, &String.to_charlist(&1)) end)
  |> Enum.map(fn sac -> Enum.at(sac, 1) -- Enum.at(sac, 1) -- Enum.at(sac, 0) end)

  # On peut encore avoir des doublons, qu'il faut supprimer
  |> Enum.map(&Enum.uniq/1)
```

<!-- livebook:{"output":true} -->

```
['p', 'L', 'P', 'v', 't', 's']
```

On doit maintenant transformer chaque chaque en la bonne valeur.
Il nous faut pour cela une fonction helper.

Ensuite, on Enum.sum avec.

```elixir
defmodule Day3Helpers.Convert do
  # Lowercase item types a through z have priorities 1 through 26.
  # Uppercase item types A through Z have priorities 27 through 52.

  def char_to_int(ch) when ch >= ?a and ch <= ?z do
    ch - ?a + 1
  end

  def char_to_int(ch) when ch >= ?A and ch <= ?Z do
    ch - ?A + 27
  end
end

step1
|> Enum.flat_map(& &1)
|> Enum.map(&Day3Helpers.Convert.char_to_int/1)
|> Enum.sum()
```

<!-- livebook:{"output":true} -->

```
157
```

## Résolution de la partie 2

Comme d'habitude, on change un peu les règles. Il s'agit maintenant de trouver les éléments communs par groupe de trois sacs ... puis d'appliquer la même conversion et somme.

Auparavant, j'ai utilisé l'opérateur de list -- pour trouver les éléments communs entre deux listes ... du coup, maintenant que nous devons opérer sur trois liste, je trouve plus élégant de passer à la généralisation via des ensembles (MapSet)

```elixir
# On split par ligne, ce qui donne des sacs, puis on doit faire des groupes de trois : 
input
|> String.split("\n", trim: true)
|> Enum.chunk_every(3)
|> Enum.map(fn sac -> Enum.map(sac, &String.to_charlist(&1)) end)

# Il sera plus élégant d'utiliser des Set pour trouvers des intersections
# de plus de deux lists, ceci via un Enum.reduce
# Attention, pour le reduce, le premier élément sert d'accumulateur

|> Enum.map(fn bagGroup ->
  [head | tail] = bagGroup
  tail |> Enum.reduce(MapSet.new(head), fn x, acc -> MapSet.intersection(acc, MapSet.new(x)) end)
end)
|> Enum.map(&MapSet.to_list/1)
|> Enum.flat_map(& &1)
|> Enum.map(&Day3Helpers.Convert.char_to_int/1)
|> Enum.sum()
```

<!-- livebook:{"output":true} -->

```
70
```
