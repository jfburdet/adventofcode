# Advent of code 2022

## Section

Cette année, je m'y colle avec un peu de retard. Evidemment le language de choix sera [Elixir](https://elixir-lang.org/).

Mes solutions sont écrites avec [LiveBook](https://livebook.dev/#install). N'hésitez pas à l'installer afin de tester et hacker mon code ci-dessous.

Voici un résumé de chaque jour :

* [Jour 1](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day1.livemd) :

  * P1 : Des groupes de chiffres : Trouver le total du groupe le plus grand
  * P2 : Trouver le total des trois groupes les plus grand.
  * Concepts utilisé : 
    * [Enum](https://hexdocs.pm/elixir/Enum.html)

* [Jour 2](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day2.livemd) :

  * C'est le jeux du feuille-caillou sciseaux.
  * P1 : On a une liste de coup et une règle pour calculer un score. Il faut trouver le total de points.
  * P2 : La sémantique de la liste de coup change : Cela n'indique plus ce qu'on doit jouer, mais si on doit gagner/perdre/nul. Je trouve l'astuce pour éviter d'implémenter les règles à l'envers, d'utiliser
     le même code, mais de jouer toutes les figures afin de trouver le résultat attendu:

    ```elixir
    |> Enum.filter(fn shape -> Day2Helpers.Outcome.round_outcome({opponent, shape}) == :win end)
    ```
  * Concepts utilisé :

    * [Enum.reduce/2](https://hexdocs.pm/elixir/Enum.html#reduce/2)
    * [Enum.filter/2](https://hexdocs.pm/elixir/Enum.html#filter/2)
    * [Fonction à multi-clauses](https://www.culttt.com/2016/05/23/multi-clause-functions-pattern-matching-guards-elixir)

* [Jour 3](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day3.livemd) :

  * P1 : On a deux groupes de lettres, on doit trouver celles qui sont communes, puis mapper ces lettres communes 
         vers des chiffres et faire la sommes. Pour cela j'utilise la propriété de l'opérateur ++, qui permet
         de trouver ce qui est commun à deux listes via : 

      ```elixir
      iex(1)> list1 = [1,2,3]
      [1, 2, 3]
      iex(2)> list2 = [4,5,2]
      [4, 5, 2]
      iex(3)> list1 -- (list1 -- list2)
      [2]        
      ```

  * P2 : Cette fois, c'est plus ce qui est commun à deux groupes, mais trois, du coup j'utilise
         [Enum.reduce/2](https://hexdocs.pm/elixir/Enum.html#reduce/2) et [MapSet](https://hexdocs.pm/elixir/MapSet.html)

* [Jour 4](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day4.livemd) :

  * L'énoncé est des paires de segments positionnels.
  * P1 : On doit trouver le nombre de paires dont l'une contient entièrement l'autre. J'ai facilement implémenté ceci par une représentation d'entier ou leur bits sont les puissances de deux des positions. Ainsi, cela donne deux nombre A et B, et l'un des deux inclu l'autre si la valeur 'A et_binaire B' vaut A ou vaut B.
  * P2 : On veut cette fois les pairs qui se chevauchent au moins partiellement. Du coup on change simplement la condition : Si le "A et_binaire B" > 0 ... c'est qu'il y avait chevauchement.
  * Concept : Opérateur binaire [&&&/2](https://hexdocs.pm/elixir/Bitwise.html#&&&/2)

* [Jour 5](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day5.livemd) :

  * L'énoncé décrit des piles de caisses entreprosées et les instructions qu'un grue doit faire pour les déplacer d'une pile à l'autre. Au sommet de chaque caisse il y a une lettre. Au terme des déplacement, on doit dire le mot que forme les code quand on regarde la pile depuis dessus.
  * P1 : Chaque ordre indique un pile source, un nombre de caisse, une pile de destination. On déplace les caisses l'une après l'autre, donc on applique une logique de pile LIFO. J'ai parsé l'énoncé comme cela, et j'applique les ordres aussi de la même façon.
  * P2 : La règle change : L'ordre donne bien le nombre de caisse, mais la grue les déplace en vrac ... il suffit d'ajouter un simple Enum.reverse_slice sur le résultat.
  * Concepts : [Enum.reverse_slice/3](https://hexdocs.pm/elixir/Enum.html#reverse_slice/3)

* [Jour 6](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day6.livemd) :  

  * On a une chaine de caractère. 
  * P1 : Il faut trouver à partiri de quel index on a une séquence de 4 caractères uniques.
    * A cette occasion, j'ai apris qu'en Elixir, quand on veut retenir un index dans un Enum, on doit décorer l'enum avec [Enum.with_indexes/2](https://hexdocs.pm/elixir/Enum.html#with_index/2) 
    * Je me souvenais qu'on pouvait faire une fenêtre glissante avec [Enum.chunk_every/4](https://hexdocs.pm/elixir/Enum.html#chunk_every/4)
    * Du coup, on boucle sur chaque chunk (de 4) qui est décoré de son index et on conserve que ceux qui sont composés de caractères uniques en faisant ```Enum.uniq(chunk) == chunk``` ... puis on prend le premier.
  * P2 : Idem, mais pour 14 caractère ... le code ayant été écrit paramétrable en P1, rien besoin de changer.
  * Concepts : 
    * [Enum.with_indexes/2](https://hexdocs.pm/elixir/Enum.html#with_index/2) 
    * [Enum.chunk_every/4](https://hexdocs.pm/elixir/Enum.html#chunk_every/4)
    * [Enum.filter/2](https://hexdocs.pm/elixir/Enum.html#filter/2)
    * Peut aussi s'implémenter en fonction bitstring. Voir le "Post-scriptum" du livebook.

* [Jour 7](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day7.livemd) :     
  * L'énoncé est une sortie de liste de commande et leur réponse d'un shell. On peut faire cd, ls et on se déplace dans l'arborescence et on voit les répertoires (nom), les fichier (+nom + taille)
  * P1 : On doit trouver les trois répetoires qui font moins de 100'000 octets et donner la somme des trois.
    * Il a fallut parser la chaine d'entrée pour en extraire les commandes (Regex)
    * Ensuite, il a fallut créer une structure hiérarchique et en accumulant toutes ces information (Enum.Reduce)
    * Finalement, on parse la structure pour trouver générer chaque répertoire et sa taille.
    * Pas facile à mettre au point.
  * P2 : On nous donne la taille totale du disque, et la taille minimum d'espace libre qu'on souhaite. On doit donner la taille du plus petit répertoire qu'on peut effacer et qui donne ceci. On ayant déjà toutes les infos en P1, c'est donc trivial
  * Concepts : 
    * [Regex](https://hexdocs.pm/elixir/Regex.html) : Expression régulière et capture. A noter qu'on peut faire des match/capture en un dans un cond, via match == Regex.run :

    ```elixir
       # check+match and capture en un seul statement
    cond do 
      match = Regex.run(~r/^\$ cd (.+)$/, line) ->
        [_, cd_dir] = match
        {:cd, cd_dir}

    ```
    * [Enum.reduce/3](https://hexdocs.pm/elixir/Enum.html#reduce/3) : Permet d'accumuler une opération sur un Enum
    * [Kernel.update_in/2](https://hexdocs.pm/elixir/1.12/Kernel.html#update_in/2) : Permet de mettre à jour un élément dans un map, mais en profondeur en indiquant le chemin de la structure de donnée. Très utile !

* [Jour 8](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day8.livemd) 
  * Une matrice carrée représente une forêt. Chaque case correspond à un arbre. Chaque case à une lettre qui représente la hauteur de l'arbre.
  * P1 : On regarde successivement depuis les 4 coté : combien d'arbre unique peut-on voir
    * J'ai dupliqué (diverses inversion + transposition), la matrice de départ, pour obtenir les vue top, right, bottom
    * Depuis chacune des vue, on établir le nombre d'arbre visible, puis on fait les opérations de transposition inverse et on somme les tableaux résultants
    ```
    View count: [
      [2, 1, 1, 3, 2],
      [1, 2, 2, 0, 1],
      [4, 1, 0, 1, 1],
      [1, 0, 2, 0, 4],
      [2, 2, 1, 4, 2]
    ]
    ```
    * Cela donne pour chaque arbre, le nombre de côté depuis le quel on le voit
    * La réponse est le nombre de case > 0

  * P2 : On change la perspective : On doit compter pour chaque arbre, non pas le nombre de côté par lequel il est vu, mais le nombre d'arbre qu'on peut voir autour de lui depuis sa position. Cela donne un indice de "belle vue", et on doit retourner la valeur du plus grand.
    * On procède finalement de la même façon, on change juste la fonction de comptage et on obtient un tableau qu'on flat_map|>max
    ```
        View count: [
      [0, 0, 0, 0, 0],
      [0, 1, 4, 1, 0],
      [0, 6, 1, 2, 0],
      [0, 1, 8, 3, 0],
      [0, 0, 0, 0, 0]
    ]
    ```
  * Concepts : 
    * [Enum.zip_with/2](https://hexdocs.pm/elixir/1.12/Enum.html#zip_with/2) : Enum.zip_with(&(&1)) -> transpose une liste de liste d'entier !
      ```
      [[1,2,3],
       [4,5,6]] |> Enum.zip_with(&(&1))

      [[1, 4],
       [2, 5],
       [3, 6]]
      ```
    * Fonction à clause multiple + pattern match + recursion + acc

* [Jour 9](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day9.livemd) 
  * Commentaire à venir.
* [Jour 10](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day10.livemd) 
  * Commentaire à venir.
* [Jour 11](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day11.livemd) 
  * Commentaire à venir.
* [Jour 12](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day12.livemd) 
  * Commentaire à venir.
* [Jour 13](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day13.livemd) 
  * Commentaire à venir.
* [Jour 14](https://livebook.dev/run?url=https://gitlab.com/jfburdet/adventofcode/-/raw/main/2022/day14.livemd) 
  * Commentaire à venir.